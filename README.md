This tool is an example of a static-code only tool for the Toolforge
environment using the build service.

See more details here:
https://wikitech.wikimedia.org/wiki/Help:Toolforge/My_first_Buildpack_static_tool
